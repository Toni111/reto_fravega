import React from 'react'
import Card from 'react-bootstrap/Card'
import '../styles/styled.css'
import { Link } from 'react-router-dom'

const Cards = ({ pokemons }) => {
  return (
    <>
      {pokemons.map((pokemon) => {
        const urlSprite =
          'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/'
        const imageId = pokemon.url.split('/')[
          pokemon.url.split('/').length - 2
        ]
        const imageUrl = `${urlSprite}${imageId}.png?raw=true`

        return (
          <Link to={`descripcion/${imageId}`}>
            <div className="card-container">
              <Card className="card">
                <Card.Img variant="top" src={imageUrl} key={imageId} />
                <Card.Body className="card-body">
                  <Card.Title className="card-title">
                    {pokemon.name}
                  </Card.Title>
                </Card.Body>
              </Card>
            </div>
          </Link>
        )
      })}
    </>
  )
}

export default Cards
