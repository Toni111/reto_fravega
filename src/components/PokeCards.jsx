import React from 'react'
import Card from './Card'
import '../styles/styled.css'
import '../fonts/style.css'
const axios = require('axios')

const RESULTS_PER_PAGE = 5

class PokeCards extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      pokemons: [],
      page: 0,
    }
    this.goToPrevPage = this.goToPrevPage.bind(this)
    this.goToNextPage = this.goToNextPage.bind(this)
  }

  componentDidMount() {
    this.apiFetch()
  }

  apiFetch = async () => {
    const { page } = this.state
    const offset = page * RESULTS_PER_PAGE
    await axios
      .get(
        `https://pokeapi.co/api/v2/pokemon/?offset=${offset}&limit=${RESULTS_PER_PAGE}`
      )
      .then((res) => {
        const { results } = res.data
        this.setState({ pokemons: results })
      })
  }

  goToPrevPage = () => {
    const { page } = this.state
    if (page === 0) {
      return
    }

    this.setState(
      {
        page: page - 1,
      },
      this.apiFetch
    )
  }
  goToNextPage = () => {
    const { page } = this.state
    this.setState(
      {
        page: page + 1,
      },
      this.apiFetch
    )
  }

  render() {
    const { pokemons } = this.state
    return (
      <div className="pokecard">
        <h1 className="btn1" onClick={this.goToPrevPage}>
          <span className="icon-cheveron-outline-left"></span>
        </h1>

        <div className="col">
          <Card pokemons={pokemons} />
        </div>
        <h1 className="btn2" onClick={this.goToNextPage}>
          <span className="icon-cheveron-outline-right"></span>
        </h1>
      </div>
    )
  }
}

export default PokeCards
