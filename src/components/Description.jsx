import React from 'react'
import Container from 'react-bootstrap/Container'

import '../styles/description.css'
import '../fonts/style.css'
import { Link } from 'react-router-dom'
const axios = require('axios')

const LanguagesEnum = {
  es: 'es',
  en: 'en',
  ja: 'ja',
}

const LanguagesLabelEnum = {
  es: 'Español',
  en: 'English',
  ja: 'Japones',
}

class Descripcion extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      descripcion: [],
      name: '',
      idioma: 'es',
    }

    this.changeLanguage = this.changeLanguage.bind(this)
  }

  componentDidMount() {
    this.fetchData()
  }

  fetchData = async () => {
    const { match } = this.props
    const res = await axios.get(
      `https://pokeapi.co/api/v2/pokemon-species/${match.params.id}/`
    )

    const x = res.data.flavor_text_entries
    this.setState({ descripcion: x })
    const y = x.filter((el) => {
      return el.language.name === `${this.state.idioma}`
    })
    this.setState({
      name: res.data,
      descripcion: y,
    })
  }

  changeLanguage = async (language) => {
    const { idioma } = this.state
    this.setState(
      {
        idioma: language,
      },
      this.fetchData
    )
  }

  render(props) {
    const { match } = this.props
    const urlSprite =
      'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/'
    const { descripcion, name, idioma } = this.state
    const id = match.params.id
    return (
      <div>
        <h1>
          <Link to="/poke-cards">
            <span className="icon-cheveron-outline-left arrow"></span>
          </Link>
        </h1>
        <Container className="container" key={id}>
          <div className="col1">
            <img
              className="img"
              src={`${urlSprite}${id}.png?raw=true`}
              alt="alt"
            ></img>{' '}
            <h3>{name.name}</h3>
          </div>

          <div className="col2">
            {' '}
            {descripcion.map((data) => (
              <h6>{data.flavor_text}</h6>
            ))}
          </div>
          <div className="btn-container">
            {Object.keys(LanguagesEnum).map((languageOption) => (
              <button
                onClick={() => this.changeLanguage(languageOption)}
                key={languageOption}
                disabled={idioma === languageOption}
                className="btn btn-danger"
              >
                {LanguagesLabelEnum[languageOption]}
              </button>
            ))}
          </div>
        </Container>
      </div>
    )
  }
}

export default Descripcion
