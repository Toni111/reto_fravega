import React from 'react'
import './App.css'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import Home from './components/Home'
import Container from './components/Container'
import Cards from './components/Card'
import Descripcion from './components/Description'
import Layout from './components/Layout'
function App() {
  return (
    <BrowserRouter>
      <Layout>
        <Route exact path="/" render={() => <Home />} />
        <Route exact path="/poke-cards" component={Container} />
        <Route exact path="/cards" component={Cards} />
        <Route
          exact
          path="/descripcion/:id"
          render={({ match }) => <Descripcion match={match} />}
        />
      </Layout>
    </BrowserRouter>
  )
}

export default App
